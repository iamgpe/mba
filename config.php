<?php

$config = [

    'application_name'      => 'Mobba Hôtel',
    'application_slogan'    => 'Slogan du rétro',
    'url'                   => 'http://localhost/mba/', // slash à la fin

    'db_host'               => 'localhost',
    'db_user'               => 'root',
    'db_password'           => '',
    'db_table'              => 'mba',

];