const path = require('path');

const config = {
    entry: path.resolve('src/js/application.js'),
    output: {
        path: path.resolve('src/js'),
        filename: 'bundle.js'
    }
};

module.exports = config;