<?php

return [
    'type' => 'website',
    'name' => 'Eurolycéepro',
    'description' => "Durant mon stage de première année en BTS, j'ai du réaliser un site web pour un consortium de 5 lycées d'occitanie. J'ai donc utilisé le CMS Wordpress pour fournir un site web de qualité le plus rapidement possible.",
    'color' => '#69a3bc',
    'logo' => false,
    'site' => 'https://eurolyceepro.org',
    'screens' => 1,
    'colors' => ['#eaeaea', '#303437', '#69a3bc', '#786b5c', '#788594'],
    'languages' => ['Wordpress', 'HTML', 'CSS'],
    'tags' => ['Wordpress', 'front-end', 'back-end'],
    'credits' => [
        'Wordpress' => 'https://wordpress.org'
    ],
    'category' => [
        'slug' => 'internship',
        'name' => 'Stages'
    ]
];