<?php

return [
    'type' => 'website',
    'name' => 'Blog',
    'description' => 'Lors des séances de cours d\'introduction aux langages du web, nous avons dû créer un petit blog pour s\'entraîner.',
    'color' => '#2e2e2f',
    'logo' => false,
    'site' => 'demo',
    'screens' => 6,
    'colors' => ['#15171a', '#738a94','#d4dee6','#2992ca','#296a8d'],
    'languages' => ['HTML', 'CSS', 'Javascript'],
    'tags' => ['HTML', 'CSS', 'Javascript', 'front-end'],
    'credits' => [
        'Google Fonts' => 'https://fonts.google.com/',
        'Font Awesome' => 'https://fontawesome.com/',
        'Unsplash' => 'https://unsplash.com/'
    ],
    'category' => [
        'slug' => 'cours',
        'name' => 'Cours'
    ]
];