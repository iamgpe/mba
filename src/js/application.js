import './mod/header'

import Swup from 'swup'
import SwupScriptsPlugin from '@swup/scripts-plugin'

const swup = new Swup({
    plugins: [
        new SwupScriptsPlugin()
    ]
})

const pages = {
    all: require('./pages/all'),
    lab: require('./pages/lab'),
    project: require('./pages/project')
}

function mount () {
    if (typeof page != 'undefined' && page in pages) {
        pages[page].mount()
    }
    
    pages.all.mount(page)
}

function unmount () {
    if (typeof page != 'undefined' && page in pages) {
        pages[page].unmount()
    }
}

swup.on('contentReplaced', mount)
swup.on('willReplaceContent', unmount)

mount()