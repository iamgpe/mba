module.exports = {

    sw_links: document.querySelectorAll('a.sw-page'),

    /**
     * Met le lien du menu correspondant à la page actuelle en avant.
     * 
     * @param {string|null} page Page visitée
     */
    mount (page = null) {
        window.scrollTo(0, 0)

        this.sw_links.forEach(e => {
            e.classList.remove('selected')
    
            if (e.classList.contains('sw-' + page)) {
                e.classList.add('selected')
            }
        })
    }

}