module.exports = {

    screens: null,
    arrow_left: null,
    arrow_right: null,
    slider: null,
    width: null,
    actual: null,
    newLeft: null,
    pow: null,
    actualLeft: null,
    time: null,

    /**
     * Met en place le javascript nécessaire pour faire fonctionner la page.
     */
    mount () {
        this.slider = document.querySelector('.slider-swww')
        this.screens = document.querySelectorAll('.slider-content')
        this.width = parseInt(getComputedStyle(this.screens[0]).width.replace('px', ''))
        this.arrow_left = document.querySelector('.arrow-slider.left')
        this.arrow_right = document.querySelector('.arrow-slider.right')
        this.actual = 1
        this.pow = false
        this.actualLeft = 0

        this.arrow_right.addEventListener('click', e => {
            this.switchTo('right')
        })
        
        this.arrow_left.addEventListener('click', e => {
            this.switchTo('left')
        })

        document.addEventListener('keydown', e => {
            if (e.isComposing || e.keyCode == 39) {
                this.switchTo('right')
            } else if (e.isComposing || e.keyCode == 37) {
                this.switchTo('left')
            }
        })
    },

    /**
     * Détruit les évenements javascript actuellement présent sur la page.
     */
    unmount () {
        this.arrow_left.removeEventListener('click')
        this.arrow_right.removeEventListener('click')
        document.removeEventListener('keydown')
    },

    /**
     * Permet de passer au contenu suivant ou précédent sur le slider.
     * 
     * @param {string} dir Direction voulue
     */
    switchTo (dir) {
        if (!this.pow) {
            this.pow = true
            this.actualLeft = parseInt(getComputedStyle(this.slider).marginLeft.replace('px', ''))

            if (dir == 'left') {
                if (this.arrow_left.classList.contains('disabled')) return false

                this.newLeft = this.actualLeft + this.width + 40
                this.actual--
            }

            if (dir == 'right') {
                if (this.arrow_right.classList.contains('disabled')) return false

                this.newLeft = this.actualLeft - this.width - 40
                this.actual++
            }

            if (this.actual <= 1) {
                this.actual = 1
                this.arrow_left.classList.add('disabled')
            } else {
                this.arrow_left.classList.remove('disabled')
            }

            if (this.actual >= this.screens.length) {
                this.actual = this.screens.length
                this.arrow_right.classList.add('disabled')
            } else {
                this.arrow_right.classList.remove('disabled')
            }

            this.slider.style.marginLeft = this.newLeft + 'px'
            
            this.screens.forEach(screen => screen.classList.remove('selected'))
            this.screens[this.actual-1].classList.add('selected')
        }
        
        this.time = setTimeout(() => { 
            this.pow = false 
        }, 500)
    }

}