module.exports = {

    links: null,
    cases: null,
    oc_category: null,

    /**
     * Met en place le javascript nécessaire pour faire fonctionner la page.
     */
    mount () {
        this.links = document.querySelectorAll('a.darina')
        this.cases = document.querySelectorAll('.items-cases-pr')

        this.links.forEach(btn => btn.addEventListener('click', e => {
            this.links.forEach(btn => btn.classList.remove('selected'))
            btn.classList.add('selected')

            this.oc_category = btn.getAttribute('on-change-category')

            this.cases.forEach(caz => {
                if (this.oc_category == 'all') {
                    caz.classList.add('visible')
                } else {   
                    if (this.oc_category == caz.getAttribute('data-category')) {
                        caz.classList.add('visible')
                    } else {
                        caz.classList.remove('visible')
                    }
                }
            })
        }))
    },

    /**
     * Détruit les évenements javascript actuellement présent sur la page.
     */
    unmount () {
        this.links.forEach(btn => btn.removeEventListener('click', btn))
    }

}