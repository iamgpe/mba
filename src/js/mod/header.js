const div = document.getElementById('navigation-fixable')

let nowPosition = 0
let newPosition = 0
let fixed = false
let transitionnable = false

const check = () => {
    if (fixed) {
        div.classList.add('fixed')
    } else {
        div.classList.remove('fixed')
    }
    
    if (transitionnable) {
        div.classList.add('transitionnable')
    } else {
        div.classList.remove('transitionnable')
    }
}

export default (swup => {

    // PAGE EVENTS 

    nowPosition = 0
    newPosition = 0
    fixed = false
    transitionnable = false

    window.addEventListener('scroll', e => {
        newPosition = window.pageYOffset
    
        if (newPosition < 1) {
            fixed = false
            transitionnable = false
        } else if (newPosition >= div.clientHeight) {
            transitionnable = true
            fixed = newPosition < nowPosition
        } else {
            transitionnable = false
        }
    
        nowPosition = newPosition
    
        check()
    }) 

})()