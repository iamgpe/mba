<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="Guillaume Perche <iamgpe@realiz.it>">
    <meta name="description" content="Passionné par le web depuis maintenant plusieurs années, je vous fait part de mes conaissances, de mon parcours et de mon avancée dans le monde d'internet via ce site web.">
    <meta name="keywords" content="iamgpe, guillaume perche, guillaume, perche, bts sio slam, bts, bts sio, sio slam, sio, slam, developpement, html, css, php, javascript">

    <meta property="og:title" content="<?= conf('application_name') . ' - ' . conf('application_slogan'); ?>">
    <meta property="og:image" content="<?= url('src/img/social-share.png'); ?>">
    <meta property="og:url" content="<?= route(''); ?>">
    <meta property="og:site-name" content="<?= conf('application_name') . ' - ' . conf('application_slogan'); ?>">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@iamgpe">
    <meta name="twitter:creator" content="@iamgpe">
    <meta name="twitter:title" content="<?= conf('application_name') . ' - ' . conf('application_slogan'); ?>">
    <meta name="twitter:description" content="Guillaume Perche. Étudiant, passionné et développeur web à mes heures perdues">
    <meta name="twitter:image:src" content="<?= url('src/img/social-share.png'); ?>">

    <meta name="theme-color" content="#e81e2e">

    <title><?= conf('application_name') . ' - ' . conf('application_slogan'); ?></title>

    <link rel="stylesheet" href="<?= url('src/css/bundle.css'); ?>">
    <link rel="shortcut icon" href="<?= url('src/images/favicon.ico'); ?>" type="image/x-icon">
</head>
<body>
    @show('content');

    <script type="text/javascript">

        if (typeof page == 'undefined') {
            var page = null;
        }
        
    </script>
    <script type="text/javascript" src="<?= url('src/js/bundle.js'); ?>"></script>
</body>
</html>