<div class="page page-about">
    <div class="content">
        <div class="top-banner global-title">
            <h2 data-content="Page introuvable" class="transition-fade">Page introuvable</h2>
            <p class="transition-fade td td1">
                Oups, il semble que la page recherchée n'existe pas ou plus... Mais une solution s'offre à vous, cliquer sur le bouton ci-dessous pour retourner à la page précédente !
            </p>
            <div class="links transition-fade td td2">
                <a href="javascript:history.go(-1)">Revenir en arrière</a>
            </div>
        </div>
    </div>
</div>