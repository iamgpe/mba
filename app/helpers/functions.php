<?php

/**
 * Remplace les points par des DIRECTORY_SEPARATOR
 * 
 * @param string $string Chaine de base
 * @return string
 */
function dotds ($string) { 
    return trim(str_replace('.', DS, $string), DS);
}

/**
 * Ajoute l'url devant une chaine de caractères
 * 
 * @param string $path Chemin 
 * @return string 
 */
function url ($path = "") {
    global $config;
    
    return $config['url'] . $path;
}

/**
 * Affiche une chaine depuis les variables de configuration
 * 
 * @param string $key Clé
 * @return string 
 */
function conf ($key) {
    global $config;
    
    return $config[$key];
}

/**
 * Récupère une route
 * 
 * @param string $name Nom de la route 
 * @return string
 */
function route ($name, $params = null) {
    $routes = App\Router\Router::list();
    $continue = true;
    $to = '';

    if (isset($routes['GET'])) {
        foreach ($routes['GET'] as $route) {
            if ($route->name == $name) {
                if (is_array($route->path)) {
                    $to = url($route->path[0]);
                    $continue = false;

                    break;
                }

                $to = url($route->path);
                $continue = false;

                break;
            }
        }
    }

    if ($continue) {
        if (isset($routes['POST'])) {
            foreach ($routes['POST'] as $route) {
                if ($route->name == $name) {
                    if (is_array($route->path)) {
                        $to = url($route->path[0]);
                        $continue = false;

                        break;
                    }

                    $to = url($route->path);
                    $continue = false;
    
                    break;
                }
            }
        }
    }

    $to = parameterDetection($to, $params);

    return ($to == '') ? route('404') : $to;
}

/**
 * Redirige vers une route
 * 
 * @param string $name Nom de la route
 * @return void
 */
function redirect ($name) {
    header('Location: ' . route($name));
}

/**
 * Vérifie si un paramètre se trouve dans le chemin de la route et remplace
 * 
 * @param string $to Chaine à analyser
 * @param array $params Paramètres à transmettre
 * @return string 
 */
function parameterDetection ($to, $params = []) {
    if ($to != '') {
        preg_match_all('/\:([a-z]+)/mi', $to, $matches, PREG_SET_ORDER, 0);

        foreach ($matches as $match) {
            if (array_key_exists($match[1], $params)) {
                $to = str_replace($match[0], $params[$match[1]], $to);
            } else {
                $to = str_replace($match[0], 'not-found', $to);
            }
        }
    }

    return $to;
}

/**
 * Affiche le chemin vers un dossier de la base de données
 * 
 * @param string $name Nom du dossier
 * @param bool $ds Avec ou sans slash à la fin
 */
function database ($name, $ds = true) {
    return DATABASE . $name . ($ds ? DS : '');
}