<?php

namespace App\Router;

use App\Router\Route as Route;
use App\Router\RouterException as RouterException;

class Router {

    /**
     * @var array $routes Liste des routes de notre application
     */
    private static $routes = [];

    /**
     * Met en place une route de type GET 
     * 
     * @param string|array $path Chemins d'accès (URL)
     * @param callback|string $controller Action à effectuer
     * @return void|null
     */
    public function get ($path, $controller) {
        return $this->addRoute('GET', $path, $controller);
    }

    /**
     * Met en place une route de type POST 
     * 
     * @param string|array $path Chemins d'accès (URL)
     * @param callback|string $controller Action à effectuer
     * @return void|null
     */
    public function post ($path, $controller) {
        return $this->addRoute('POST', $path, $controller);
    }

    /**
     * Met en place une route 
     * 
     * @param string $type Type d'accès à la route (GET ou POST)
     * @param string|array $path Chemins d'accès (URL)
     * @param callback|string $controller Action à effectuer
     * @return void|null
     */
    private function addRoute ($type, $path, $controller) {
        $route = new Route();

        $route->setType($type);
        $route->setPath($path);
        $route->setController($controller);

        self::$routes[$type][] = $route;

        return $route;
    }

    /**
     * Mise en route de notre router
     * 
     * @return void|null
     */
    public function run () {
        if (!isset(self::$routes[$_SERVER['REQUEST_METHOD']])) {
            redirect('404');
        }

        foreach (self::$routes[$_SERVER['REQUEST_METHOD']] as $route) {
            $gurl = $_GET['url'] ?? '/';

            if (preg_match_all('/\:([a-z]+)/mi', $route->path, $matches, PREG_SET_ORDER, 0)) {
                $path = explode('/', $route->path);
                $url = explode('/', $gurl);

                if ($path[0] == $url[0] && substr($path[1], 0, 1) == ':') {
                    $_GET[ltrim($path[1], ':')] = $url[1];
                    
                    $gurl = str_replace($url[1], $path[1], $gurl);
                }
            }

            if ($route->match($gurl)) {
                return $route->call();
            }
        }

        redirect('404');
    }

    /**
     * Liste toutes les routes
     * 
     * @return array 
     */
    public function list () {
        return self::$routes;
    }

}