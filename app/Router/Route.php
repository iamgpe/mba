<?php

namespace App\Router;

class Route {

    /** 
     * @var string $name Alias de la route
     */
    public $name;

    /**
     * @var string|array $path Chemin-s d'accès
     */
    public $path;
    
    /**
     * @var string type $type Type d'accès à notre route
     */
    public $type;

    /**
     * @var string $controller Controller à appeler
     */
    public $controller;

    /**
     * @var array $matches Différents matchs trouvés pour la route
     */
    private $matches;

    /**
     * Définit le nom de la route actuelle, ce qui nous permettra de modifier l'url n'importe quand sans forcément tout toucher dans le code de l'application.
     * 
     * @return self
     */
    public function setName ($name) { 
        $this->name = $name;

        return $this;
    }

    /**
     * Définit le ou les chemins d'accès à la route actuelle
     * 
     * @return self
     */
    public function setPath ($path) { 
        $this->path = trim($path, '/');

        return $this;
    }

    /**
     * Définit le type d'accès vers notre route
     * 
     * @return self
     */
    public function setType ($type) { 
        $this->type = $type;

        return $this;
    }

    /**
     * Définit le controller notre route
     * 
     * @return self
     */
    public function setController ($controller) { 
        $this->controller = $controller;

        return $this;
    }

    /**
     * Vérifie la route
     * 
     * @param string $url Lien à vérifier
     * @return void|null
     */    
    public function match ($url) {
        $url = trim($url, '/');

        if ($url == $this->path) return true;

        return false;
    }

    /**
     * Appelle soit le controller lié à la route actuelle et le lance, soit invoque la fonction anonyme sauvegardée sur la route actuelle
     * 
     * @return class
     */
    public function call () {
        if (is_string($this->controller)) {
            try {
                $controller = explode('@', $this->controller)[0];
                $method = explode('@', $this->controller)[1];
                $class = "App\\Controllers\\{$controller}Controller";

                return (new $class())->{$method}();
            } catch (Exception $e) {
                throw new Exception($e);
            }
        } else if (is_callable($this->controller)) {
            return call_user_func($this->controller);
        }
    }

}