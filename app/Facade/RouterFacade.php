<?php

namespace App\Facade;

use App\Router\Router as Router;

class RouterFacade {

    public static function __callStatic ($method, $arguments) {
        $router = new Router();

        return call_user_func_array([$router, $method], $arguments);
    }

}