<?php

namespace App\Core;

class Helpers {

    /**
     * Hash une chaine de caractère
     * 
     * @param string $string Chaine de caractère à hasher
     * @return string
     */
    public function hash ($string) {
        $param = '$' . implode('$', ['2y', str_pad(10, 2, conf('security_strpadchar'), STR_PAD_LEFT), conf('security_salt')]);
        
        return crypt($string, $param);
    }

    /**
     * Regarde si une adresse email est valide ou non
     * 
     * @param string $email Adresse email à vérifier
     * @return bool
     */
    public function isEmail ($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email);
    }

    /**
     * Formate une date en fonction d'un timestamp
     * 
     * @param string $time Date à traiter
     * @param string $lang Langue (FR ou autre)
     * @return string 
     */
    public function dateFormat ($time, $lang = 'EN') {
        $format = ($lang == 'FR') ? 'd/m/Y H:i:s' : 'Y/m/d H:i:s';

        return date($format, $time);
    }

    /**
     * Corrige les différents textes en retirant les caractères parasites
     * 
     * @param string $value Contenu à corriger
     * @param bool $keepInternSpace On garde les espaces internes
     * @return string
     */
    public function noParasite ($value, $keepInternSpace = true) {
        $value = trim($value);
        $value = str_replace('~\x{00a0}~', ' ', $value);

        if (!$keepInternSpace) {
            $value = str_replace('/\s+/', '', $value);
        }

        return $value;
    }

}
