<?php

namespace App\Core;

use App\Core\Helpers as Helpers;

class Session {

    /**
     * Créer une session
     * 
     * @param string $key Clé de la session
     * @param string $value Valeur de la session 
     */
    public static function set ($key, $value) {
        $_SESSION[$key] = $value;
    }

    /**
     * Récupère le contenu d'une session
     * 
     * @param string $key Clé de la session
     */
    public static function get ($key) {
        return $_SESSION[$key] ?? null;
    }

    /**
     * Supprime une session
     * 
     * @param string $key Clé de la session
     */
    public static function destroy ($key) {
        self::set($key, null);
        
        unset($_SESSION[$key]);
    }

    /**
     * Vérifie l'existence d'une session
     * 
     * @param string $key Clé de la session
     * @return bool
     */
    public static function exist ($key) {
        return self::get($key) !== null;
    }

    /**
     * Vérifie l'existence de plusieurs sessions
     * 
     * @param string ...$key Clés des sessions
     * @return bool
     */
    public static function exists (...$key) {
        $exist = true;

        foreach ($key as $session) {
            if (!self::exist($session)) {
                $exist = false;
            }
        }

        return $exist;
    }

}