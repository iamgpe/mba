<?php

namespace App\Core;

class Database
{

    /**
     * @var string $table Table avec laquelle notre système va communiquer
     */
    protected $table;

    /**
     * @var array $where Conditions
     */
    protected $where = [];

    /**
     * @var array $select Champs à récupérer
     */
    protected $select = [];

    /**
     * @var \PDO $pdo Stocke l'instance de PDO
     */
    protected $pdo;

    /**
     * @var mixed $request Stocke la requête SQL
     */
    protected $request = null;

    /**
     * @var int $limit Limite d'informations à récupérer
     */
    protected $limit = 0;

    /**
     * @var array $order Ordre d'affichage
     */
    protected $order = [];

    public function __construct()
    {
        try {
            $this->pdo = new \PDO("mysql:dbname=" . conf('db_name') . ";host=" . conf('db_host'), conf('db_user'), conf('db_pass'), [
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES => false,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
            ]);
        } catch (\PDOException $e) {
            throw $e;
        }
    }

    /**
     * Réinitialise les résulats précédents. Cette méthode permet surtout de régler les
     * problèmes de conflits entre les résultats des requêtes qui ont eu lieues dans une
     * même classe sans nouvelle instance.
     */
    public function reset()
    {
        $this->where = [];
        $this->select = [];
        $this->request = null;
    }

    /**
     * Définit la limite de données à envoyer.
     *
     * @param int $limit nombre de données à retourner
     * @return $this
     */
    public function limit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * Ajoute une condition à notre requête SQL.
     *
     * @param mixed ...$conditions
     * @return $this
     */
    public function where(...$conditions)
    {
        foreach ($conditions as $condition) {
            $this->where[] = $condition;
        }

        return $this;
    }

    /**
     * Définit un tri dans un certain ordre par rapport à un certain champs.
     * d
     * @param string $input Nom du champ
     * @param string $type Type de tri (ASC ou DESC)
     * @return $this
     */
    public function orderBy($input, $type)
    {
        $type = strtoupper($type);
        $type = ($type != 'DESC' && $type != 'ASC') ? 'DESC' : $type;

        $this->order = [$input, $type];

        return $this;
    }

    /**
     * Mise en place d'une requête de type SELECT.
     *
     * @return \PDOStatement
     */
    public function select()
    {
        $conditions = [];
        $params = [];

        $inputs = (count($this->select) > 0) ? join(', ', $this->select) : '*';
        $limit = ($this->limit != 0) ? ('LIMIT ' . $this->limit) : '';
        $order = ($this->order != []) ? 'ORDER BY ' . $this->order[0] . ' ' . $this->order[1] : '';

        foreach ($this->where as $where) {
            $buildWhere = $this->buildWhere($where);

            $conditions[] = $buildWhere['conditions'];
            $params = $this->addParams($params, $buildWhere['params']);
        }

        $conditions = (count($conditions) > 0) ? 'WHERE (' . join(') OR (', $conditions) . ')' : '';
        $request = join(' ', ['SELECT', $inputs, 'FROM', $this->table, $conditions, $order, $limit]);

        return $this->do($request, $params);
    }

    /**
     * Modifie la table actuelle.
     *
     * @param string $table Nom de la table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * Préparation des requêtes.
     *
     * @param string $query Requête SQLe
     * @param array $params Valeurs des paramètres
     * @return \PDOStatement
     */
    public function do($query, $params = [])
    {
        try {
            if ($params != []) {
                $statement = $this->pdo->prepare($query);

                $statement->execute($params);
            } else {
                $statement = $this->pdo->query($query);
            }

            return $statement;
        } catch (\PDOException $e) {
            die($e);
        }
    }

    /**
     * Retourne le contenu de la requête
     *
     * @param bool $xss Sécurise ou non notre retour
     * @return mixed
     */
    public function get($xss = true)
    {
        $results = $this->select()->fetchAll(\PDO::FETCH_ASSOC);

        if ($xss) {
            array_walk_recursive($results, [__CLASS__, 'xss']);
        }

        $this->reset();

        return $results;
    }

    /**
     * Compte le nombre d'entrées
     *
     * @param string $input Champs à compoter
     * @return int
     */
    public function count($input = 'id')
    {
        $this->select[] = "COUNT($input)";

        $result = $this->select()->fetch(\PDO::FETCH_COLUMN);

        $this->reset();

        return $result;
    }

    /**
     * Protège contre les failles XSS
     *
     * @param string $string Chaine à sécuriser
     * @return string
     */
    private function xss($string)
    {
        return htmlspecialchars($string, ENT_QUOTES);
    }

    /**
     * Ajout d'une entrée dans la table
     *
     * @param array $params
     * @return bool
     */
    public function insert($params)
    {
        $keys = join(',', array_keys($params));
        $values = ':' . join(', :', array_keys($params));
        $query = join(' ', ['INSERT INTO', $this->table, '(', $keys, ') VALUES (', $values, ')']);

        if ($this->do($query, $params)) {
            return true;
        }

        return false;
    }

    /**
     * Modification d'une entrée dans la table
     *
     * @param string $input Nom du champ à modifier
     * @param string $newValue Nouvelle valeur du champ $input
     * @param string $where Conditions de modification
     */
    public function update($input, $newValue, $where = '')
    {
        $params = [$input => $newValue];
        $conditions = [];

        if (is_array($where)) {
            $buildWhere = $this->buildWhere($where);
            $conditions[] = $buildWhere['conditions'];

            $params = $this->addParams($params, $buildWhere['params']);
        }

        $conditions = (count($conditions) < 1) ? '' : 'WHERE ' . join(' AND', $conditions);
        $query = join(' ', ['UPDATE', $this->table, 'SET', $input, '=', ':' . $input, $conditions]);

        $this->do($query, $params);
    }

    /**
     * Supprime une entrée
     *
     * @param string $where Conditions de modification
     */
    public function remove($where = '')
    {
        $params = [];
        $conditions = [];

        if (is_array($where)) {
            $buildWhere = $this->buildWhere($where);
            $conditions[] = $buildWhere['conditions'];

            $params = $this->addParams($params, $buildWhere['params']);
        }

        $conditions = (count($conditions) < 1) ? '' : 'WHERE ' . join(' AND ', $conditions);
        $query = join(' ', ['DELETE FROM', $this->table, $conditions]);

        $this->do($query, $params);
    }

    /**
     * Ajout d'une condition dans notre requête.
     *
     * @param array $where Condition à analyser
     * @return array
     */
    private function buildWhere($where)
    {
        $condition = [];
        $params = [];

        foreach ($where as $key => $value) {
            $keyUnique = $key . uniqid();
            $operation = substr($value, 0, 1);
            $value = ($value == null) ? '' : $value;

            if ($operation == '>' || $operation == '>=' || $operation == '<' || $operation == '<=') {
                $sign = $operation;
                $value = str_replace($sign . ' ', '', $value);
            } else {
                $sign = '=';
            }

            $params[$keyUnique] = $value;
            $condition[] = "$key $sign :$keyUnique";
        }

        return [
            'conditions' => '(' . join(') AND (', $condition) . ')',
            'params' => $params
        ];
    }

    /**
     * Ajout de paramètres dans la requête SQL.
     *
     * @param array $params Liste des paramètres actuels
     * @param array $new_params Paramètres à ajouter dans la liste
     * @return mixed
     */
    private function addParams($params, $new_params)
    {
        foreach ($new_params as $key => $value) {
            $params[$key] = $value;
        }

        return $params;
    }

}