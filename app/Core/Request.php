<?php

namespace App\Core;

use App\Core\Helpers as Helpers;

class Request {

    /**
     * Type de la méthode utilisée
     * 
     * @var string $method 
     */
    private $method;

    /**
     * Variables passées
     * 
     * @var array $variables
     */
    private $variables;

    /**
     * Constructeur
     *
     * @return void|null
     */
    public function __construct () {
        $this->method = $_SERVER['REQUEST_METHOD'];

        $this->getAllVariables();
    }

    /**
     * Lance un helper via la requête
     * 
     * @return App\Core\Helpers
     */
    public function helper () {
        return new Helpers();
    }

    /**
     * Récupères toutes les variables de paramètre
     * 
     * @return void|null
     */
    private function getAllVariables () {
        $this->variables = $_REQUEST;

        array_shift($this->variables);
    }

    /**
     * Récupère une variable passée 
     * 
     * @param string $name Clé de la variable
     * @return mixed
     */
    public function input ($key) {
        return Helpers::noParasite($_REQUEST[$key]);
    }

    /**
     * Vérifie si des champs sont vides
     * 
     * @param array $inputs Les champs à vérifier
     * @return bool
     */
    public function empty (...$inputs) {
        foreach ($inputs as $input) {
            if (empty($this->input($input))) {
                return true;
                break;
            }
        }

        return false;
    }

}