<?php

namespace App\Core;

use App\Views\View as View;

class Controller {

    /**
     * Page par défaut
     * 
     * @var string|null $master 
     */
    protected $master = 'master';

    /**
     * Charge une vue présente dans le dossier Views
     * 
     * @param string $path Chemin de la vue à récupérer, peut être séparé par un .
     * @param array $variables Variables à passer à la vue qui va être chargée
     * @return void|null
     */
    protected function render ($path, $variables = []) {
        $path = dotds(strtolower($path));
        
        new View($path, $this->master, $variables);
    }

    /**
     * Modifie le header actuel
     * 
     * @param string $type Type de page
     * @return void|null
     */
    protected function setHeader ($type) {
        header('Content-Type: ' . $type);
    }

    /**
     * Renvoi des données dans un format JSON
     * 
     * @param array $values valeurs retournées
     * @return string
     * 
     */
    protected function JSONResults ($values, $echo = true) {
        if ($echo) echo json_encode($values, JSON_UNESCAPED_UNICODE);
        
        return json_encode($values, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Redirige un utilisateur vers une route spécifique
     * 
     * @param string $name Nom de la route
     */
    protected function redirect ($name) {
        route($name);
    }

}