<?php

namespace App\Models;

use App\Core\Database as Database;

class User extends Database {

    protected $table = 'users';

    private $exist;

    /**
     * Constructeur
     * 
     * @param string $key Clé du champ à sélectionner
     * @param string $value Valeur du champ à sélectionner
     */
    public function __construct ($key, $value) {
        parent::__construct();

        $user_req = $this->where([ $key => $value ])->get();

        if (count($user_req) < 1) {
            $this->exist = false;
        } else {
            $agent = extract($user_req[0]);

            $this->exist = true;
            
        }
    }

    /**
     * Retourne si l'utilisateur existe ou non
     * 
     * @return bool
     */
    public function exist () {
        return $this->exist;
    }

    /**
     * Créer un nouvel utilisateur
     * 
     * @param string ...$informations Informations de l'utilisateur à inscrire
     * @return void|null 
     */
    public function create (...$test) {
        return 'ok';
    }

}