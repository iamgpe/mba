<?php

namespace App\Controllers;

use App\Core\Controller as Controller;

class ApplicationController extends Controller
{

    protected $template = 'default';

    protected $defaultMessage = 'Une erreur est survenue. Merci de recharger la page et de réessayer.';

}