<?php

namespace App\Controllers;

use App\Controllers\ApplicationController as ApplicationController;

class ErrorController extends ApplicationController
{

    /**
     * Affiche le contenu de la page d'erreur
     */
    public function show()
    {
        $this->render('not-found-404');
    }

}