<?php

namespace App\Controllers;

use App\Controllers\ApplicationController as ApplicationController;
use App\Models\User as User;
use App\Core\Session as Session;

class LoginController extends ApplicationController
{

    /**
     * Affiche le contenu de la page d'accueil
     */
    public function show()
    {
        if (!Session::exists('username', 'password')) {
            $this->render('login');
        } else {
            $this->redirect('profile');
        }
    }

}