<?php

namespace App\Views;

class ViewReplacement {

    /**
     * Met en route une directive
     * 
     * @param string $name Nom de la directive
     * @param mixed $replace Contenu à remplacer
     * @param mixed $subject Contenu à analyser
     * @return mixed
     */
    public function directive($name, $replace, $subject) {
        return call_user_func_array([__NAMESPACE__ . '\ViewReplacement', 'directive_' . $name], [$replace, $subject]);
    }

    /**
     * Met en route la directive show
     * 
     * @param mixed $replace Contenu à remplacer
     * @param mixed $subject Contenu à analyser
     * @return mixed
     */
    private function directive_show ($replace, $subject) {
        $re = '/@show\(\'([\w_-]+)\'\);/im';

        preg_match_all($re, $subject, $matches);

        if (count($matches) > 0) {
            return preg_replace($re, $replace, $subject);
        }

        return $subject;
    }

}