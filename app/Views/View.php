<?php

namespace App\Views;

use App\Views\ViewReplacement as ViewReplacement;

class View {

    /**
     * Contient le chemin vers nos vues
     * 
     * @var string $path 
     */
    private $path = ROOT . DS . 'app' . DS . 'templates' . DS;
    
    /**
     * Variables qui seront lancés lors des includes
     * 
     * @var array $variables 
     */
    private $variables;

    /**
     * Constructeur
     * 
     * @param string $path Chemin de la vue à charger
     * @param string|null $master Page par défaut
     * @param array $variables Variables
     * @return void|null
     */
    public function __construct ($path, $master, $variables) {
        $this->variables = $variables;
        
        $master_content = $this->obGetContent($master);
        $page_content = $this->obGetContent($path);

        $page_content = ViewReplacement::directive('show', $page_content, $master_content);

        echo $page_content;
    }

    /**
     * Récupère le contenu d'une vue
     * 
     * @param string $path Chemin vers le fichier
     * @return mixed
     */
    private function obGetContent ($path) {
        ob_start();
        extract($this->variables);
        require($this->path . dotds($path) . '.php');

        $content = ob_get_contents();

        ob_end_clean();

        return $content;
    }

}