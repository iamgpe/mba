<?php

session_start();

require_once './config.php';
require_once './app/helpers/constants.php';
require_once './app/helpers/functions.php';
require_once './vendor/autoload.php';

use App\Facade\RouterFacade as Router;

Router::get('/', 'Login@show')->setName('login');
Router::get('/404', 'Error@show')->setName('404');

Router::run();